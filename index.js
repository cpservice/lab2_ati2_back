const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const path = require('path');
require('./configdatabase');

app.use(cors());
app.options("*", cors());
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
app.use(methodOverride('_method'));
app.use(require('./routes'));

app.use(express.static(path.join(__dirname,"Public")));
app.get("*", (req,res) => {
    res.sendFile(path.join(__dirname,'/public/index.html'));
});

app.listen(8001, () => {
    console.log('Server listen on port', 8001);
});