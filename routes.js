const express = require('express');
const app = express();

const Student = require('./models/Student');

app.get('/getall', async (req, res) => {
    await Student.find({}).exec((err, students) => {
        if(err){
            return res.json({
                ok: false
            });
        }else {
            return res.json({
                ok: true,
                students
            });
        }
    });

});

app.post('/create', async (req, res) => {
    let body = req.body;
    console.log(body);

    let student = new Student({
        name: body.name,
        email: body.email,
        phone: body.phone,
        identification_number: body.identification_number,
        university: body.university,
        career: body.career
    });

    console.log('-----------------');
    console.log(student);

    await student.save({runValidators: true}, (err, student) => {
        if(err){
            console.log(err);
            return res.json({
                ok: false,
            });
        } else {
            return res.json({
                ok: true,
                student
            });
        }
    });

});

app.get('/show/:id', async (req, res) => {
    let id = req.params.id;

    await Student.findById(id).exec((err, student) => {
        if(err){
            return res.json({
                ok: false,
            });
        } else {
            if(!student){
                return res.json({
                    ok: false,
                    error: 'Student not found',
                });
            } else {
                return res.json({
                    ok: true,
                    student
                });
            }
        }
    });
});

app.post('/update/:id', async (req, res) => {
    let id = req.params.id

    let body = {};
    if (req.body.name) body.name = req.body.name;
    if (req.body.email) body.email = req.body.email;
    if (req.body.phone) body.phone = req.body.phone;
    if (req.body.identification_number) body.identification_number = req.body.identification_number;
    if (req.body.university) body.university = req.body.university;
    if (req.body.career) body.career = req.body.career;
    
    body = { $set: body }

    await Student.findByIdAndUpdate(id,  body, {new: true, runValidators: true}, (err, student) => {
        if(err){
            console.log(err);
            return res.json({
                ok: false,
            });
        } else {
            if(!student){
                return res.json({
                    ok: false,
                    error: 'Student not found',
                });
            } else {
                return res.json({
                    ok: true,
                    student
                });
            }
        }
    });

});

app.delete('/delete/:id', async (req, res) => {
    let id = req.params.id;

    await Student.findByIdAndDelete(id, (err, student) => {
        if(err){
            return res.json({
                ok: false,
            });
        } else {
            if(!student){
                return res.json({
                    ok: false,
                    error: 'Student not found',
                });
            } else {
                return res.json({
                    ok: true,
                });
            }
        }
    });
});




//exports
module.exports = app;