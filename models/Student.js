const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let StudentSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true,
        match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    },
    phone: {
        type: String,
        unique: true,
        required: true
    },
    identification_number: {
        type: Number,
        required: true
    },
    university: {
        type: String,
        required: true,
    },
    career: {
        type: String,
        required: true,
    }
}, {
    versionKey: false
});

//exports
module.exports = mongoose.model('Student', StudentSchema);