const mongoose = require('mongoose');

mongoose.connect("mongodb://localhost:27017/laboratorio2", {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})
    .then(db => console.log('Base de datos conectada'))
    .catch(err => console.log(err));